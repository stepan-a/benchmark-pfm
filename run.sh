#!/usr/bin/env bash

WHEREAMI=$(dirname $(realpath "$0"))

if [ ! -v ITERATIONS ]
then
    echo "Set default number of iterations (niter=1)."
    ITERATIONS=1
else
    echo "Number of iterations is niter=$ITERATIONS."
fi

if [ ! -v NOFAST ]
then
    echo "Use fast option (with use_dll)."
    NOFAST=false
else
    echo "Do not use fast option (with use_dll)"
fi

if [ ! -v DYNARE ]
then
    echo "Path to dynare must be provided."
fi

if [ $# -eq 0 ]
then
    echo "The name of one model at least is required."
else
    if [ $# -eq 1 ]
    then
        echo "I will run perfect foresight simulations with 1 model:"
    else
        echo "I will run perfect foresight simulations with $# models:"
    fi
fi

for model in "$@"
do
    printf " \u25A1 $model\n"
done

for model in "$@"
do
    if [ ! -f $model-done.info ]
    then
        while [ ! -f $model-done.info ]
        do
            matlab -batch "addpath $DYNARE; addpath $WHEREAMI/matlab; MODEL='$model'; niter=$ITERATIONS; nofast=$NOFAST; run_all_simulations;" |& tee -a $model-all.log
        done
        printf " \u2714 $model\n"
    else
        printf " \u2714 $model (already done)\n"
    fi
done
