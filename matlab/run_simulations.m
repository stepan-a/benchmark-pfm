function run_simulations(model, stack_solve_algo, static_solve_algo, niter, ccrit, nofast)

% Run perfect foresight model with different options and algorithms.
%
% INPUTS
% - model               [char]      1×n array, name of the mod file (without extension).
% - stack_solve_algo    [integer]   1×p array, identifiers for the solver of the stack model.
% - static_solve_algo   [integer]   1×q array, identifiers for the solver of the static model (only used with stack_solve_algo==7)
% - niter               [integer]   scalar, number of simulation of each configuration.
% - ccrit               [double]    scalar, precision threshold.
% - nofast              [logical]   scalar, do not use fast option if true.
%
% OUTPUTS
% None
%
% REMARKS
% Results (timings) are stored in a .mat file named simulations-{model}.mat.

% Copyright © 2024 Dynare Team

global oo_

dynare_config;
tmp = strsplit(dynare_version, {'.','-'});
dynare_version_major = str2num(tmp{1});

if niter==0
    error('Fourth argument (number of iterations) cannot be equal to zero.')
end

weight = @(n) (n<0) + (n>0)*(1/n);

if nargin<6
    nofast = false;
end

id = 1:length(stack_solve_algo);
jd = 1:length(static_solve_algo);

%
% Initialise the arrys storing the timings
%
Timings = matfile(sprintf('simulations-%s.mat', model));
if exist(sprintf('simulations-%s.mat', model), 'file')
    Timings.Properties.Writable = true;
end
if ~ismember('MATLAB_computing', {whos(Timings).name})
    Timings.MATLAB_preprocessing = NaN(length(stack_solve_algo), length(static_solve_algo));
    Timings.MATLAB_computing = NaN(length(stack_solve_algo), length(static_solve_algo));
end
if ~ismember('USE_DLL_computing', {whos(Timings).name})
    Timings.USE_DLL_preprocessing = NaN(length(stack_solve_algo), length(static_solve_algo));
    Timings.USE_DLL_computing = NaN(length(stack_solve_algo), length(static_solve_algo));
end
if ~ismember('BYTECODE_computing', {whos(Timings).name})
    Timings.BYTECODE_preprocessing = NaN(length(stack_solve_algo), 1);
    Timings.BYTECODE_computing = NaN(length(stack_solve_algo), 1);
end
if ~ismember('MATLAB_WITH_BLOCKS_computing', {whos(Timings).name})
    Timings.MATLAB_WITH_BLOCKS_preprocessing = NaN(length(stack_solve_algo), 4);
    Timings.MATLAB_WITH_BLOCKS_computing = NaN(length(stack_solve_algo), 4);
end
if ~ismember('USE_DLL_WITH_BLOCKS_computing', {whos(Timings).name})
    Timings.USE_DLL_WITH_BLOCKS_preprocessing = NaN(length(stack_solve_algo), 4);
    Timings.USE_DLL_WITH_BLOCKS_computing = NaN(length(stack_solve_algo), 4);
end
if ~ismember('BYTECODE_WITH_BLOCKS_computing', {whos(Timings).name})
    Timings.BYTECODE_WITH_BLOCKS_preprocessing = NaN(length(stack_solve_algo), 4);
    Timings.BYTECODE_WITH_BLOCKS_computing = NaN(length(stack_solve_algo), 4);
end
%
% Matlab
%
for algo = stack_solve_algo
    if ismember(algo, [4 5]) || (ismember(algo, [2 3]) && dynare_version_major <= 6)
        % These algorithms are not available
        Timings.MATLAB_preprocessing(id(stack_solve_algo==algo), 1) = -10;
        Timings.MATLAB_computing(id(stack_solve_algo==algo), 1) = -10;
        continue
    end
    if algo<7
        if isnan(Timings.MATLAB_computing(id(stack_solve_algo==algo), 1))
            % Try to simulate the model if this configuration has not been tried before (with or without success).
            try
                disp_title(sprintf('| MATLAB (solve_algo=%u, no blocks, model=%s) |', algo, model));
                dprintf('dynare %s onlyclearglobals -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u', model, abs(niter), algo);
                skipline()
                Timings.MATLAB_computing(id(stack_solve_algo==algo), 1) = -3; % Provision for MATLAB crash
                info = dynare(sprintf('%s', model), 'onlyclearglobals', sprintf('-DITERATIONS=%u', abs(niter)), sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
                if oo_.deterministic_simulation.error<ccrit
                    Timings.MATLAB_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
                    Timings.MATLAB_computing(id(stack_solve_algo==algo), 1) = info.time.compute*weight(niter);
                else
                    Timings.MATLAB_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
                    Timings.MATLAB_computing(id(stack_solve_algo==algo), 1) = -2;
                end
            catch E
                fprintf(1, 'Simulation failed:\n\n%s', E.message);
                Timings.MATLAB_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
                Timings.MATLAB_computing(id(stack_solve_algo==algo), 1) = -1;
            end
        end
    else
        for solver = static_solve_algo
            if isnan(Timings.MATLAB_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)))
                % Try to simulate the model if this configuration has not been tried before (with or without success).
                try
                    disp_title(sprintf('| MATLAB (solve_algo=%u, no blocks, model=%s) |', algo, model));
                    dprintf('dynare %s onlyclearglobals -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u -DSTEADY_SOLVE_ALGO_VALUE=%u', model, abs(niter), algo, solver);
                    skipline()
                    Timings.MATLAB_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = -3; % Provision for MATLAB crash
                    info = dynare(sprintf('%s', model), 'onlyclearglobals', sprintf('-DITERATIONS=%u', abs(niter)), sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo), sprintf('-DSTEADY_SOLVE_ALGO_VALUE=%u', solver));
                    if oo_.deterministic_simulation.error<ccrit
                        Timings.MATLAB_preprocessing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.preprocessor;
                        Timings.MATLAB_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.compute*weight(niter);
                    else
                        Timings.MATLAB_preprocessing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.preprocessor;
                        Timings.MATLAB_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = -2;
                    end
                catch E
                    fprintf(1, 'Simulation failed:\n\n%s', E.message);
                    Timings.MATLAB_preprocessing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.preprocessor;
                    Timings.MATLAB_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = -1;
                end
            end
        end
    end
end
%
% use_dll
%
for algo = stack_solve_algo
    if ismember(algo, [4 5]) || (ismember(algo, [2 3]) && dynare_version_major <= 6)
        % These algorithms are not available
        Timings.USE_DLL_preprocessing(id(stack_solve_algo==algo), 1) = -10;
        Timings.USE_DLL_computing(id(stack_solve_algo==algo), 1) = -10;
        continue
    end
    if algo<7
        try
            if isnan(Timings.USE_DLL_computing(id(stack_solve_algo==algo), 1))
                disp_title(sprintf('| USE_DLL (solve_algo=%u, no blocks, model=%s) |', algo, model));
                if nofast
                    dprintf('dynare %s onlyclearglobals -DUSE_DLL=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u', model, abs(niter), algo);
                else
                    dprintf('dynare %s onlyclearglobals fast -DUSE_DLL=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u', model, abs(niter), algo);
                end
                skipline()
                Timings.USE_DLL_computing(id(stack_solve_algo==algo), 1) = -3; % Provision for MATLAB crash
                if nofast
                    info = dynare(sprintf('%s', model), 'onlyclearglobals', '-DUSE_DLL=true', sprintf('-DITERATIONS=%u', abs(niter)), sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
                else
                    info = dynare(sprintf('%s', model), 'onlyclearglobals', 'fast', '-DUSE_DLL=true', sprintf('-DITERATIONS=%u', abs(niter)), sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
                end
                if oo_.deterministic_simulation.error<ccrit
                    Timings.USE_DLL_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
                    Timings.USE_DLL_computing(id(stack_solve_algo==algo), 1) = info.time.compute*weight(niter);
                else
                    Timings.USE_DLL_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
                    Timings.USE_DLL_computing(id(stack_solve_algo==algo), 1) = -2;
                end
            end
        catch E
            fprintf(1, 'Simulation failed:\n\n%s', E.message);
            Timings.USE_DLL_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
            Timings.USE_DLL_computing(id(stack_solve_algo==algo), 1) = -1;
        end
    else
        for solver = static_solve_algo
            try
                if isnan(Timings.USE_DLL_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)))
                    disp_title(sprintf('| USE_DLL (solve_algo=%u, no blocks, model=%s) |', algo, model));
                    if nofast
                        dprintf('dynare %s onlyclearglobals -DUSE_DLL=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u -DSTEADY_SOLVE_ALGO_VALUE=%u', model, abs(niter), algo, solver);
                    else
                        dprintf('dynare %s onlyclearglobals fast -DUSE_DLL=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u -DSTEADY_SOLVE_ALGO_VALUE=%u', model, abs(niter), algo, solver);
                    end
                    skipline()
                    Timings.USE_DLL_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = -3; % Provision for MATLAB crash
                    if nofast
                        info = dynare(sprintf('%s', model), 'onlyclearglobals', '-DUSE_DLL=true', sprintf('-DITERATIONS=%u', abs(niter)), sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo), sprintf('-DSTEADY_SOLVE_ALGO_VALUE=%u', solver));
                    else
                        info = dynare(sprintf('%s', model), 'onlyclearglobals', 'fast', '-DUSE_DLL=true', sprintf('-DITERATIONS=%u', abs(niter)), sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo), sprintf('-DSTEADY_SOLVE_ALGO_VALUE=%u', solver));
                    end
                    if oo_.deterministic_simulation.error<ccrit
                        Timings.USE_DLL_preprocessing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.preprocessor;
                        Timings.USE_DLL_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.compute*weight(niter);
                    else
                        Timings.USE_DLL_preprocessing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.preprocessor;
                        Timings.USE_DLL_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = -2;
                    end
                end
            catch E
                fprintf(1, 'Simulation failed:\n\n%s', E.message);
                Timings.USE_DLL_preprocessing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = info.time.preprocessor;
                Timings.USE_DLL_computing(id(stack_solve_algo==algo), jd(solver==static_solve_algo)) = -1;
            end
        end
    end
end
%
% Bytecode
%
for algo = stack_solve_algo
    try
        if isnan(Timings.BYTECODE_computing(id(stack_solve_algo==algo), 1))
            disp_title(sprintf('| BYTECODE (solve_algo=%u, no blocks, model=%s) |', algo, model));
            dprintf('dynare %s onlyclearglobals -DBYTECODE=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u', model, abs(niter), algo);
            skipline()
            Timings.BYTECODE_computing(id(stack_solve_algo==algo), 1) = -3; % Provision for MATLAB crash
            info = dynare(sprintf('%s', model), 'onlyclearglobals', sprintf('-DITERATIONS=%u', abs(niter)), '-DBYTECODE=true', sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
            if oo_.deterministic_simulation.error<ccrit
                Timings.BYTECODE_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
                Timings.BYTECODE_computing(id(stack_solve_algo==algo), 1) = info.time.compute*weight(niter);
            else
                Timings.BYTECODE_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
                Timings.BYTECODE_computing(id(stack_solve_algo==algo), 1) = -2;
            end
        end
    catch E
        fprintf(1, 'Simulation failed:\n\n%s', E.message);
        Timings.BYTECODE_preprocessing(id(stack_solve_algo==algo), 1) = info.time.preprocessor;
        Timings.BYTECODE_computing(id(stack_solve_algo==algo), 1) = -1;
    end
end
%
% Matlab with blocks
%
for mfs = 0:3
    for algo = stack_solve_algo
        if algo == 5
            % Algorithm not available
            Timings.MATLAB_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = -10;
            Timings.MATLAB_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -10;
            continue
        end
        if isnan(Timings.MATLAB_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1))
            try
                disp_title(sprintf('| MATLAB (solve_algo=%u, with blocks, mfs=%u, model=%s) |', algo, mfs, model));
                dprintf('dynare %s onlyclearglobals -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u -DMFS_VALUE=%u -DBLOCKS=true', model, abs(niter), algo, mfs);
                skipline()
                Timings.MATLAB_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -3; % Provision for MATLAB crash
                info = dynare(sprintf('%s', model), 'onlyclearglobals', sprintf('-DITERATIONS=%u', abs(niter)), sprintf('-DMFS_VALUE=%u', mfs), '-DBLOCKS=true', sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
                if oo_.deterministic_simulation.error<ccrit
                    Timings.MATLAB_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                    Timings.MATLAB_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = info.time.compute*weight(niter);
                else
                    Timings.MATLAB_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                    Timings.MATLAB_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -2;
                end
            catch E
                fprintf(1, 'Simulation failed:\n\n%s', E.message);
                Timings.MATLAB_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                Timings.MATLAB_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -1;
            end
        end
    end
end
%
% USE_DLL with blocks
%
for mfs = 0:3
    for algo = stack_solve_algo
        if algo == 5
            % Algorithm not available
            Timings.USE_DLL_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = -10;
            Timings.USE_DLL_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -10;
            continue
        end
        if isnan(Timings.USE_DLL_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1))
            try
                disp_title(sprintf('| USE_DLL (solve_algo=%u, with blocks, mfs=%u, model=%s) |', algo, mfs, model));
                if nofast
                    dprintf('dynare %s onlyclearglobals -DUSE_DLL=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u -DMFS_VALUE=%u -DBLOCKS=true', model, abs(niter), algo, mfs);
                else
                    dprintf('dynare %s onlyclearglobals fast -DUSE_DLL=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u -DMFS_VALUE=%u -DBLOCKS=true', model, abs(niter), algo, mfs);
                end
                skipline()
                Timings.USE_DLL_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -3; % Provision for MATLAB crash
                if nofast
                    info = dynare(sprintf('%s', model), 'onlyclearglobals', sprintf('-DITERATIONS=%u', abs(niter)), '-DBLOCKS=true', sprintf('-DMFS_VALUE=%u', mfs), '-DUSE_DLL=true', sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
                else
                    info = dynare(sprintf('%s', model), 'onlyclearglobals', 'fast', sprintf('-DITERATIONS=%u', abs(niter)), '-DBLOCKS=true', sprintf('-DMFS_VALUE=%u', mfs), '-DUSE_DLL=true', sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
                end
                if oo_.deterministic_simulation.error<ccrit
                    Timings.USE_DLL_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                    Timings.USE_DLL_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = info.time.compute*weight(niter);
                else
                    Timings.USE_DLL_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                    Timings.USE_DLL_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -2;
                end
            catch E
                fprintf(1, 'Simulation failed:\n\n%s', E.message);
                Timings.USE_DLL_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                Timings.USE_DLL_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -1;
            end
        end
    end
    system(sprintf('rm -rf %s', model));
    system(sprintf('rm -rf +%s', model));
end
%
% Bytecode with blocks
%
for mfs = 0:3
    for algo = stack_solve_algo
        if isnan(Timings.BYTECODE_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1))
            try
                disp_title(sprintf('| BYTECODE (solve_algo=%u, with blocks, mfs=%u, model=%s) |', algo, mfs, model));
                dprintf('dynare %s onlyclearglobals -DBYTECODE=true -DITERATIONS=%u -DSTACK_SOLVE_ALGO_VALUE=%u -DMFS_VALUE=%u -DBLOCKS=true', model, abs(niter), algo, mfs);
                skipline()
                Timings.BYTECODE_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -3; % Provision for MATLAB crash
                info = dynare(sprintf('%s', model), 'onlyclearglobals', sprintf('-DITERATIONS=%u', abs(niter)), '-DBLOCKS=true', sprintf('-DMFS_VALUE=%u', mfs), '-DBYTECODE=true', sprintf('-DSTACK_SOLVE_ALGO_VALUE=%u', algo));
                if oo_.deterministic_simulation.error<ccrit
                    Timings.BYTECODE_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                    Timings.BYTECODE_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = info.time.compute*weight(niter);
                else
                    Timings.BYTECODE_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                    Timings.BYTECODE_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -2;
                end
            catch E
                fprintf(1, 'Simulation failed:\n\n%s', E.message);
                Timings.BYTECODE_WITH_BLOCKS_preprocessing(id(stack_solve_algo==algo), mfs+1) = info.time.preprocessor;
                Timings.BYTECODE_WITH_BLOCKS_computing(id(stack_solve_algo==algo), mfs+1) = -1;
            end
        end
    end
end
%
% Touch a file if everything is done.
%
system(sprintf('touch %s-done.info', model))

function disp_title(txt)
len = length(txt);
skipline()
dprintf('+%s+\n%s\n+%s+', repmat('-', 1, len-2), txt, repmat('-', 1, len-2))
skipline()
