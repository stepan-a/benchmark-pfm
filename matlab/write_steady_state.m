function write_steady_state(output, DynareModel, DynareOutput)

% Write the steady state into a file (endogenous and exogenous variables)
%
% INPUTS
% - output        [char]   row array, name of the generated file
% - DynareModel   [struct] aka M_
% - DynareOutput  [struct] aka oo_

% Copyright (C) 2024 Dynare Team

fid = fopen(output, 'w');
fprintf(fid, '// File generated on %s.', char(datetime));
skipline(2, fid)
for i=1:DynareModel.orig_endo_nbr
    fprintf(fid, '%s = %f;\n', DynareModel.endo_names{i}, DynareOutput.steady_state(i));
end
skipline(2, fid)
for i=1:DynareModel.exo_nbr
    fprintf(fid, '%s = %f;\n', DynareModel.exo_names{i}, DynareOutput.exo_steady_state(i));
end
fclose(fid);
