% Copyright (C) 2024 Dynare Team

%{
stack_solve_algo =
0 : Use a Newton algorithm with a direct sparse LU solver at each iteration, 
    applied on the stacked system of all the equations at every period (Default).
1 : Use the Laffargue-Boucekkine-Juillard (LBJ) algorithm proposed in Juillard (1996). 
    It is slower than stack_solve_algo=0, but may be less memory consuming on big models. 
    Note that if the block option is used (see Model declaration), a simple Newton 
    algorithm with sparse matrices is used for blocks which are purely backward or 
    forward (of type SOLVE BACKWARD or SOLVE FORWARD, see model_info), since LBJ only 
    makes sense on blocks with both leads and lags (of type SOLVE TWO BOUNDARIES).
2 : Use a Newton algorithm with a Generalized Minimal Residual (GMRES) solver 
    at each iteration (requires bytecode and/or block option, see Model declaration)
3 : Use a Newton algorithm with a Stabilized Bi-Conjugate Gradient (BICGSTAB) 
    solver at each iteration (requires bytecode and/or block option, see Model declaration).
4 : Use a Newton algorithm with an optimal path length at each iteration (requires 
    bytecode and/or block option, see Model declaration).
5 : Use a Newton algorithm with a sparse Gaussian elimination (SPE) solver at 
    each iteration (requires bytecode option, see Model declaration).
6 : Synonymous for stack_solve_algo=1. Kept for historical reasons.
7 : Allows the user to solve the perfect foresight model with the solvers available 
    through option solve_algo (See solve_algo for a list of possible values, 
    note that values 5, 6, 7 and 8, which require bytecode and/or block options, 
    are not allowed).
%}

nopng = false;

stack_solve_algo = [0, 1, 2, 3, 4, 5];

static_solve_algo = [0, 1, 2, 3, 4, 9, 13];

if ~exist('niter', 'var')
    % Set default for the number of iterations.
    niter = 1;
end

if ~exist('ccrit', 'var')
    % Set default precision.
    ccrit = 1e-5;
end

if ~exist('nofast', 'var')
    % Set default precision.
    nofast = false;
end

if ~exist('MODEL', 'var')
    error('Unspecified model. Please set variable MODEL (name of a mod file, without extension).');
end

if ~exist(sprintf('%s.mod', MODEL), 'file')
    error('File %s.mod is not in the current directory.', MODEL);
end

run_simulations(MODEL, stack_solve_algo, static_solve_algo, niter, ccrit, nofast);

texname = sprintf('%s-%s', MODEL, lower(regexprep(char(java.net.InetAddress.getLocalHost.getHostName),'.local\>',''))); % TODO: Check if we need a trap for Octave.

write_table_short(sprintf('simulations-%s.mat', MODEL), texname, nopng, stack_solve_algo)

% Delete files and subfolders
delete(sprintf('%s.log', MODEL))
try % Do not fail if directories have already been removed (e.g. when only regenerating the tables)
    rmdir(sprintf('%s', MODEL), 's')
catch
end
try
    rmdir(sprintf('+%s', MODEL), 's')
catch
end

% Move generated table in the current folder
p = strrep(which('run_all_simulations'), sprintf('matlab%srun_all_simulations.m', filesep), sprintf('tables%s', filesep));
movefile(sprintf('%s%s.pdf', p, texname), pwd)
movefile(sprintf('%s%s.tex', p, texname), pwd)
if ~nopng
    movefile(sprintf('%s%s.png', p, texname), pwd)
end
