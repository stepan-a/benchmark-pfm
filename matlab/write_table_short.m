function write_table_short(datafile, texfilename, nopng, stack_solve_algo)

% Produce a table of results with LaTex compiled as a pdf file and converted to a png file.
%
% INPUTS
% - datafile      [char]   1×n array, name of a .mat (without extension)
% - texfilename   [char]   1×m array, name of the .{tex,png,pdf} file containing the table of results.
%
% OUTPUTS
% None

% Copyright (C) 2024 Dynare Team

if nargin<3
    nopng = true;
end

cfolder = pwd;

D = load(datafile); % Must be in a local variable since we have a nested function below

[folder,~, ~] = fileparts(mfilename('fullpath'));
idf = strfind(folder, filesep());
rootfolder = folder(1:idf(end));
fid = fopen(sprintf('%stables/template-short.tex', rootfolder),'r');
T = fread(fid, '*char')';
fclose(fid);

function replace_template_field(field_name, value)
    if ~isnan(value)
        switch value
            case -1
                T = strrep(T, field_name, 'crash');  % code crash
            case -2
                T = strrep(T, field_name, 'FAIL');   % algorithm did not converge
            case -3
                T = strrep(T, field_name, 'CRASH');  % matlab crash
            case -10
                T = strrep(T, field_name, 'NA');  % not available
            otherwise
                T = strrep(T, field_name, int2str(value));
        end
    end
end

%
% Matlab
%
for algo = stack_solve_algo
    replace_template_field(sprintf('{a%u_mat}', algo), D.MATLAB_computing(find(stack_solve_algo==algo), 1));
end
%
% use_dll
%
for algo = stack_solve_algo
    replace_template_field(sprintf('{a%u_dll}', algo), D.USE_DLL_computing(find(stack_solve_algo==algo), 1));
end
%
% Bytecode
%
for algo = stack_solve_algo
    replace_template_field(sprintf('{a%u_byt}', algo), D.BYTECODE_computing(find(stack_solve_algo==algo), 1));
end
%
% Matlab with blocks
%
for mfs = 0:3
    for algo = stack_solve_algo
        replace_template_field(sprintf('{a%u_mat_blk_%u}', algo, mfs), ...
                               D.MATLAB_WITH_BLOCKS_computing(find(stack_solve_algo==algo), mfs+1));
    end
end
%
% USE_DLL with blocks
%
for mfs = 0:3
    for algo = stack_solve_algo
        replace_template_field(sprintf('{a%u_dll_blk_%u}', algo, mfs), ...
                               D.USE_DLL_WITH_BLOCKS_computing(find(stack_solve_algo==algo), mfs+1));
    end
end
%
% Bytecode with blocks
%
for mfs = 0:3
    for algo = stack_solve_algo
        replace_template_field(sprintf('{a%u_byt_blk_%u}', algo, mfs), ...
                               D.BYTECODE_WITH_BLOCKS_computing(find(stack_solve_algo==algo), mfs+1));
    end
end

%
% Save table
%

fid  = fopen( sprintf('%stables/%s.tex', rootfolder, texfilename), 'w');
fprintf(fid, '%s', T);
fclose(fid);

%
% Compile to pdf and png
%

cd(sprintf('%stables', rootfolder));
system(sprintf('pdflatex %s', texfilename));
if ~nopng
    system(sprintf('convert -density 600 %s.pdf %s.png', texfilename, texfilename));
end
cd(cfolder);


%
% Cleanup
%

cd(sprintf('%stables', rootfolder));
system(sprintf('rm %s.log', texfilename));
system(sprintf('rm %s.aux', texfilename));
cd(cfolder);

end
